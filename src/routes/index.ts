import express, { Express, Request, Response, Router, NextFunction } from 'express';
import middleware from '../modules/middleware';
import {RestaurantsRepository} from '../models/Restaurants'

var router: Router = express.Router()

/* GET all restaurants */
router.get('/', async function(req: Request, res: Response, next: NextFunction) {
    const restaurants = await RestaurantsRepository.find();
    if(restaurants != null){
        res.send(restaurants);
    }        
    else{
        res.sendStatus(404);
    } 
});

/**** SECURE ZONE ****/
router.use(middleware)

/* GET a restaurant */
router.get('/:id', async function(req: Request, res: Response, next: NextFunction) {
    try {
        const restaurants = await RestaurantsRepository.findOne({ _id: req.params.id });
        if(restaurants != null){
            res.send(restaurants);
        }        
        else{
            res.sendStatus(404);
        } 
    } catch (error){
        console.log(error)
		res.status(404).send({ error: "Restaurant doesn't exist!" })
	}
});

/* GET a restaurant */
router.get('/user/:id', async function(req: Request, res: Response, next: NextFunction) {
    try {
        const restaurants = await RestaurantsRepository.findOne({ owner: req.params.id });
        if(restaurants != null){
            res.send(restaurants);
        }        
        else{
            res.sendStatus(404);
        } 
    } catch (error){
        console.log(error)
		res.status(404).send({ error: "Restaurant doesn't exist!" })
	}
});

/* POST a restaurant */
router.post('/', async function(req: Request, res: Response, next: NextFunction) {
    if(JSON.parse(req.body.user.role).includes("CUSTOMER")){
        console.log(await RestaurantsRepository.findOne({owner: req.body.user.id}))
        if((await RestaurantsRepository.findOne({owner: req.body.user.id})) == null){
            const post = new RestaurantsRepository({
                name: req.body.name,
                owner: req.body.user.id,
                address: req.body.address,
                schedules: req.body.schedules,
                rate: req.body.rate,
                tags: req.body.tags,
                image: req.body.image
            })
            await post.save()
            res.send(post)
        }else{
            res.status(400).send({error: "User already have a restaurant"})
        }
    }else{
        res.sendStatus(403);
    }
});

/* UPDATE a restaurant */
router.put('/:id', async function(req: Request, res: Response, next: NextFunction) {
    try {
		const restaurant = await RestaurantsRepository.findOne({ _id: req.params.id })
        if(req.body.user.id == restaurant.owner && JSON.parse(req.body.user.role).includes("CUSTOMER")){
            restaurant.name = req.body.name || restaurant.name;
            restaurant.address = req.body.address || restaurant.address;
            restaurant.schedules = req.body.schedules || restaurant.schedules;
            restaurant.rate = req.body.rate || restaurant.rate;
            restaurant.tags = req.body.tags || restaurant.tags;
            restaurant.image = req.body.image || restaurant.image;
            await restaurant.save()
            res.send(restaurant)
            return
        }
        res.sendStatus(403);
	} catch (error){
        console.log(error)
		res.status(404).send({ error: "Restaurant doesn't exist!" })
	}
});
/* DELETE a restaurant */
router.delete('/:id', async function(req: Request, res: Response, next: NextFunction) {
    try {
        const restaurant = await RestaurantsRepository.findOne({ _id: req.params.id })
        const roles = JSON.parse(req.body.user.role)
        if((req.body.user.id == restaurant.owner && roles.includes("CUSTOMER"))  || roles.includes("EMPLOYEE")){
            await RestaurantsRepository.deleteOne({ _id: req.params.id })
            res.status(204).send()
            return
        }
        res.sendStatus(403);
	} catch (error) {
        console.log(error)
		res.status(404).send({ error: "Restaurant doesn't exist!" })
	}
});

export default router;
